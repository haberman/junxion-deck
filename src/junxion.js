class Queue {
  constructor() {
    this.oldest_index = 1
    this.newest_index = 1
    this.storage = {}
  }

  size() {
    return this.newest_index - this.oldest_index
  }

  enqueue(data) {
    this.storage[this.newest_index] = data
    this.newest_index++
  }

  dequeue() {
    let oldest_index = this.oldest_index
    let newest_index = this.newest_index
    let deleted

    if (oldest_index !== newest_index) {
      deleted = this.storage[oldest_index]

      delete this.storage[oldest_index]
      this.oldest_index++

      return deleted
    }
  }
}

export class Junxion {
  constructor(uid, connections, lng, lat) {
    this.uid = uid
    this.connections = connections
    this.lng = lng
    this.lat = lat

    this.crossed = false
    this.crossed_at = 0

    this.distance_from_parent = 0
    this.depth = 0

    this.parent = null
    this.children = []
  }
}

export class Tree {
  constructor(junxion) {
    this.root = junxion
  }

  print() {
    let depth = 0
    let children = this.root.children

    while (children.length !== 0) {
      depth++
      children = children[0].children
    }

    console.log(depth)
  }

  deepest_random_child() {
    let children = this.root.children
    let child = this.random_child(children)

    while (children.length !== 0) {
      child = this.random_child(children)
      children = child.children
    }

    return child
  }

  random_child(children) {
    let idx = Math.floor(Math.random() * children.length)
    return children[idx]
  }

  add(junxion, to) {
    let parent = null

    const callback = j => {
      if (j.uid === to.uid) {
        parent = j
      }
    }

    this.traverseBF(callback)

    if (parent) {
      parent.children.push(junxion)
      junxion.parent = parent
      junxion.depth = parent.depth + 1
    } else {
      throw new Error('Cannot add node to a non-existent parent.')
    }

  }

  remove(junxion, from) {
    let parent = null

    const callback = j => {
      if (j.uid === from.uid) {
        parent = j
      }
    }

    this.traverseBF(callback)

    if (parent) {
      const index = parent.children.findIndex(k => k.uid === junxion.uid)
      return parent.children.splice(index, 1)
    } else {
      throw new Error('Parent does not exist.')
    }
  }

  traverseBF(callback) {
    const queue = new Queue()
    queue.enqueue(this.root)

    let current_tree = queue.dequeue()
    while (current_tree) {
      for (let child of current_tree.children) { queue.enqueue(child) }

      callback(current_tree)
      current_tree = queue.dequeue()
    }

  }

  traverseDF(callback) {
    const recurse = (junxion) => {
      for (let child of junxion.children) {
        recurse(child)
        callback(junxion)
      }
    }

    recurse(this.root)

  }
}
