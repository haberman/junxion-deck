/// app.js
import React, {
  Component
} from 'react'

import {
  render
} from 'react-dom'

import MapGL from 'react-map-gl'
import DeckGLOverlay from './deckgl-overlay.js'

import * as xhr from './xhr.js'
import * as turf from '@turf/turf'

import {
  number_map
} from './fn.js'

import {
  Junxion,
  Tree
} from './junxion.js'

const CITY = 'charleroi'
const MAX_ITERATION = 10
const SPEED = 1500 // m/s
const JUNXIONS_GEOJSON = `./data/${CITY}-8.geojson`

const DIRECTION_API = 'https://api.mapbox.com/directions/v5/mapbox/driving'

class Root extends Component {

  constructor(props) {
    super(props)

    this.state = {
      viewport: {
        latitude: 50.410809,
        longitude: 4.444643,
        zoom: 12,
        width: 500,
        height: 500
      },
      time: {
        start: null,
        progress: null
      },
      trips: null
    }

    this.junxions = []
    this.tree = null
    this.iteration = 0
    this.clock = 0
    this.loop_length = 0
    this.vendor = 'a'
  }

  componentDidMount() {
    window.addEventListener('resize', this._resize.bind(this))
    this._resize()
    this._load_features()
  }

  componentWillUnmount() {
    if (this._animationFrame) {
      window.cancelAnimationFrame(this._animationFrame);
    }
  }

  render() {
    const {
      viewport,
      trips,
      time
    } = this.state

    const map_param = Object.assign({}, viewport, {
      mapStyle: 'mapbox://styles/penumbra/cjmc1y4yz58ag2sp66a5zwn5p',
      onViewportChange: this._on_viewport_change.bind(this),
      mapboxApiAccessToken: process.env.MAPBOX_ACCESS_TOKEN
    })

    const deck_param = {
      viewport: viewport,
      trips: trips,
      trailLength: 2000,
      time: time.progress
    }

    return React.createElement(MapGL, map_param, React.createElement(DeckGLOverlay, deck_param))

  }

  _uncrossed_junxions_count() {
    let uncrossed = 0

    this.junxions.forEach(junxion => {
      if (!junxion.crossed) { uncrossed++ }
    })

    return uncrossed
  }

  async _load_features() {
    const feature_collection = await xhr.call(JUNXIONS_GEOJSON)

    for (let i = 0; i < feature_collection.features.length; ++i) {
      const feature = feature_collection.features[i]
      const uid = feature.properties.uid
      const connections = feature.properties.connections
      const lng = feature.geometry.geometries[0].coordinates[0]
      const lat = feature.geometry.geometries[0].coordinates[1]

      this.junxions.push(new Junxion(uid, connections, lng, lat))
    }

    // this._populate_binary()

    const root = this._random_junxion()
    root.crossed = true

    this.tree = new Tree(root)
    this._populate_tree(root)

    this._animate()

    return true
  }

  /**
   * Distribute every nodes along 2 vendors track
   */
  _populate_binary() {
    let a = this._random_junxion()
    a.crossed = true

    let b = this._random_junxion()
    b.crossed = true

    const a_segment = [
      [a.lng, a.lat, 0]
    ]
    const b_segment = [
      [b.lng, b.lat, 0]
    ]

    let next = null
    let duration = 0

    while (this._uncrossed_junxions_count() > 0) {
      if (this.vendor === 'a') {
        next = this._nearest_junxion(a)
        next.crossed = true

        duration = (next.distance_from_parent * 1000) / (SPEED / 100) + a_segment[a_segment.length - 1][2]
        a_segment.push([next.lng, next.lat, duration])
        a = next

        this.vendor = 'b'

      } else if (this.vendor == 'b') {
        next = this._nearest_junxion(b)
        next.crossed = true

        duration = (next.distance_from_parent * 1000) / (SPEED / 100) + b_segment[b_segment.length - 1][2]
        b_segment.push([next.lng, next.lat, duration])
        b = next

        this.vendor = 'a'
      }
    }

    this.setState({
      viewport: Object.assign(this.state.viewport, {
        longitude: a_segment[0][0],
        latitude: a_segment[0][1]
      }),
      trips: [{
        vendor: 'a',
        segments: a_segment
      }, {
        vendor: 'b',
        segments: b_segment
      }],
      time: {
        start: Date.now(),
        progress: 0
      }
    })
  }

  _populate_tree(junxion) {
    for (let i = 0; i < 1; i++) {
      const nearest = this._nearest_junxion(junxion)
      nearest.crossed = true
      this.tree.add(nearest, junxion)
    }

    const c = junxion.children.reduce((acc, val) => acc + val.connections, 0)

    if (this._uncrossed_junxions_count() - c > 0) {
      const next_junxion = this.tree.deepest_random_child()
      this._populate_tree(next_junxion)
    } else {
      this._populate_trip()
    }
  }

  _populate_trip() {
    const trips = []

    this.tree.traverseBF(junxion => {
      if (junxion.depth != 0) {
        const trip = this._trip_member(trips, junxion.parent.uid)

        trip.segments.push([junxion.parent.lng, junxion.parent.lat, junxion.parent.crossed_at])

        // for ( let child of junxion.children ) {
        //   let duration = ( child.distance_from_parent * 1000 ) / ( SPEED / 1000 )
        //   if ( trip.segments.length > 0 ) {
        //     duration += trip.segments[ trip.segments.length - 1 ][ 2 ]
        //   } else {
        //     duration += child.parent.crossed_at
        //   }
        //
        //   child.crossed_at = duration
        //   trip.segments.push( [ child.lng, child.lat, duration ] )
        // }

        let duration = (junxion.distance_from_parent * 1000) / (SPEED / 100)
        if (trip.segments.length > 0) {
          duration += trip.segments[trip.segments.length - 1][2]
        } else {
          duration = junxion.parent.crossed_at
        }

        junxion.crossed_at = duration
        this.loop_length = Math.max(duration, this.loop_length)
        trip.segments.push([junxion.lng, junxion.lat, duration])
      }

    })


    this.setState({
      trips: trips,
      viewport: Object.assign(this.state.viewport, {
        longitude: this.tree.root.lng,
        latitude: this.tree.root.lat
      }),
      time: {
        start: Date.now(),
        progress: 0
      }
    })
  }

  _trip_member(trips, uid) {
    let member = trips.find(trip => trip.uid === uid)

    if (!member) {
      member = {
        uid: uid,
        segments: []
      }

      trips.push(member)
    }

    return member
  }


  // _attach_trip( junxion ) {
  //   if ( !this.state.trips ) {
  //     this.state.trips = []
  //   }
  //
  //   if ( this.state )
  //
  //     const trip = {
  //       segments: []
  //     }
  //
  //   let clock = clock_start
  //   console.log( `clock : ${clock}` );
  //   for ( const step of route.steps ) {
  //     trip.segments.push( [ step.lng, step.lat, clock ] )
  //     clock += step.duration * SPEED
  //   }
  //
  //   this.state.trips.push( trip )
  //   console.log( `trips amount: ${this.state.trips.length}` )
  // }


  async _start_sequence(s) {
    this._junxions_count()

    const start = s || this._random_junxion()
    const clock_start = start.crossed_at

    console.log(`starting sequence from ${clock_start}`)
    const sequence = []

    for (let i = 0; i < start.connections; i++) {
      const nearest = this._nearest_junxion(s)
      nearest.crossed = true
    }

    // for ( let i = 0; i < start.connections; i++ ) {
    //   const route = await this._load_route( start )
    //
    //   if ( route !== null ) {
    //     sequence.push( route )
    //     this._build_trip( route, clock_start )
    //   }
    // }
    //
    // if ( this.iteration < MAX_ITERATION ) {
    //   for ( const route of sequence ) {
    //     this.iteration += 1
    //     this._start_sequence( route.travel.end )
    //   }
    // } else if ( !this.state.time.start ) {
    //   this.state.time.start = Date.now()
    //   this._animate()
    // }

  }

  async _load_route(start) {
    const end = this._uncrossed_junxion()

    const url = `${DIRECTION_API}/${start.lng},${start.lat};${end.lng},${end.lat}`
    const params = `steps=true&access_token=${process.env.MAPBOX_ACCESS_TOKEN}`

    const direction = await xhr.call(`${url}?${params}`)
    if (direction.code != 'Ok') {
      return null
    }

    const steps = direction.routes[0].legs[0].steps
    if (steps.length == 0) {
      return null
    }

    const duration = direction.routes[0].duration
    end.crossed_at = start.crossed_at + duration

    const route = {
      travel: {
        start: start,
        end: end
      },
      duration: duration,
      steps: []
    }

    for (let i = 0; i < step.length; ++i) {
      const step = steps[i]
      const location = step.maneuver.location

      if (this._junxion_at_location(location[0], location[1]) !== null) {
        console.log(`this step overlaps a junxion`);
        break;
      }

      route.steps.push({
        lng: location[0],
        lat: location[1],
        duration: step.duration
      })
    }

    return route
  }

  _animate() {
    const timestamp = Date.now() / 1000
    const loopLength = 1800
    const loopTime = loopLength / 30

    const progress = ((timestamp % loopTime) / loopTime) * loopLength

    this.setState({
      time: Object.assign(this.state.time, {
        progress: progress
      })
    })

    this._animationFrame = window.requestAnimationFrame(this._animate.bind(this));
  }

  _uncrossed_junxions() {
    let uncrossed = []

    for (let i = 0; i < this.junxions.length; i++) {
      let junxion = this.junxions[i]

      if (!junxion.crossed) {
        uncrossed.push(junxion)
      }
    }

    return uncrossed
  }

  _random_junxion() {
    const junxions = this._uncrossed_junxions()

    // Auo reset
    if (junxions.length == 0) {
      for (let i = 0; i < this.junxions.length; i++) {
        this.junxions[i].crossed = false
      }

      return this._random_junxion()
    }

    const idx = Math.floor(Math.random() * junxions.length)
    return this._junxion_by_uid(junxions[idx].uid)
  }

  _nearest_junxion(j) {
    const junxion = j || this._random_junxion()
    junxion.crossed = true

    const junxions = this._uncrossed_junxions()

    if (junxions.length == 0) {
      return null
    }

    const point_from = turf.point([junxion.lng, junxion.lat])

    let nearest = null
    let min_distance = Infinity

    for (let i = 0; i < junxions.length; ++i) {
      const uncrossed = junxions[i]
      const point_to = turf.point([uncrossed.lng, uncrossed.lat])
      const distance = turf.distance(point_from, point_to)
      if (distance < min_distance) {
        min_distance = distance
        nearest = uncrossed
      }
    }

    nearest.distance_from_parent = min_distance
    return nearest
  }

  _junxion_by_uid(uid) {
    return this.junxions.find((junxion) => junxion.uid === uid)
  }

  _resize() {
    this._on_viewport_change({
      width: window.innerWidth,
      height: window.innerHeight
    })
  }

  _on_viewport_change(viewport) {
    this.setState({
      viewport: Object.assign(this.state.viewport, viewport)
    });
  }
}

render(React.createElement(Root), document.body.appendChild(document.createElement('div')))
